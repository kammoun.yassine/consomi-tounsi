package esprit.pi.config;

public interface SecurityParams {
	 public static final String JWT_HEADER_NAME="Authorization";
	    public static final String SECRET="kammoun.yassine@esprit.tn";
	    public static final long EXPIRATION=864_000_000;
	    public static final String HEADER_PREFIX="Bearer ";

}
