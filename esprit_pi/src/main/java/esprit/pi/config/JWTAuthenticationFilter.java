package esprit.pi.config;


import java.io.IOException;
import java.io.Writer;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;

import com.auth0.jwt.JWT;
import com.auth0.jwt.algorithms.Algorithm;
import com.fasterxml.jackson.databind.ObjectMapper;

import esprit.pi.entities.UserConsomi;



public class JWTAuthenticationFilter extends UsernamePasswordAuthenticationFilter  {
   @Autowired	
   AuthenticationManager authenticationManager;

    public JWTAuthenticationFilter(AuthenticationManager authenticationManager) {
        this.authenticationManager = authenticationManager;
    }



    @Override
    public Authentication attemptAuthentication(HttpServletRequest request, HttpServletResponse response) throws AuthenticationException {
        try {
            
        	UserConsomi appUser= new ObjectMapper().readValue(request.getInputStream(),UserConsomi.class);
            
        	System.out.println("******");
        	System.out.println("username:"+appUser.getUsername());
        	System.out.println("username:"+appUser.getPassword());
       
        	return authenticationManager.authenticate(new UsernamePasswordAuthenticationToken(appUser.getUsername(),appUser.getPassword()));
        } catch (IOException e) {
            e.printStackTrace();
            throw new RuntimeException(e);
        }
    }

   @Override
   protected void successfulAuthentication(HttpServletRequest request, HttpServletResponse response,
       FilterChain chain, Authentication authResult) throws IOException,
   ServletException {
       User user=(User)authResult.getPrincipal();
       List<String> roles=new ArrayList<>();
        authResult.getAuthorities().forEach(a->{
           roles.add(a.getAuthority());
     });
       String jwt=JWT.create()
               .withIssuer(request.getRequestURI())
               .withSubject(user.getUsername())
                .withArrayClaim("roles",roles.toArray(new String[roles.size()]))
                .withExpiresAt(new Date(System.currentTimeMillis()+SecurityParams.EXPIRATION))
               .sign(Algorithm.HMAC256(SecurityParams.SECRET));
       response.addHeader(SecurityParams.JWT_HEADER_NAME,jwt);
    
   }
  
}
