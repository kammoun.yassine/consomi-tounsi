package esprit.pi.config;

import org.springframework.beans.factory.annotation.Autowired;

import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import java.util.ArrayList;
import java.util.Collection;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;

import esprit.pi.entities.UserConsomi;
import esprit.pi.repository.UserRepository;
import esprit.pi.services.IUserService;

@Service
@JsonDeserialize
public class MyUserDetailsService implements UserDetailsService{
	@Autowired
	IUserService iuserservice;
	@Autowired
	UserRepository userRepository;
	@Override
	public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
		UserConsomi user = iuserservice.loadUserByUsername(username);
        if(user==null) throw new UsernameNotFoundException(username);
        Collection<GrantedAuthority> authorities= new ArrayList<>();
       user.getRole().forEach(r->{
           authorities.add(new SimpleGrantedAuthority(r.getRole()));
        });
       return  new User(user.getUsername(),user.getPassword(),authorities);
	}

}
