package esprit.pi.entities;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import java.io.Serializable;
import java.util.Objects;

@Embeddable
public class UserProductRatingKey implements Serializable {
    @Column(name = "id")
    private int id;

    @Column(name = "product_asin")
    private int asin;

    public UserProductRatingKey() {
    }

    public UserProductRatingKey(int id, int asin) {
        this.id = id;
        this.asin = asin;
    }

    public int getuserId() {
        return id;
    }

    public void setUserId(int  id) {
        this.id = id;
    }

    public int getASIN() {
        return asin;
    }

    public void setAsin(int asin) {
        this.asin = asin;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }

        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        UserProductRatingKey that = (UserProductRatingKey) o;
        return Objects.equals(id, that.id) &&
                Objects.equals(asin, that.asin);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, asin);
    }
}
