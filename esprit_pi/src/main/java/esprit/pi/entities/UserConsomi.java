package esprit.pi.entities;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;

import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;

import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.OneToMany;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;


import com.fasterxml.jackson.databind.annotation.JsonDeserialize;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.Getter;
import lombok.NoArgsConstructor;

@Entity
@Data
@AllArgsConstructor
@NoArgsConstructor
@Getter
@Builder
@JsonDeserialize
public class UserConsomi implements Serializable{

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int User_id;
	//@NotEmpty
	//@Size(min=3,max=40,message="nom tres court ressayez a nouveau")
	private String username;
	
	//@NotEmpty
	//@Size(min=3,max=40,message="last name tres court ressayez a nouveau")
	private String last_name;
	private String Adress;
	private String Login;
	@javax.validation.constraints.Email
	private String Email;
	//@Size(min=6,message="mots de passe tres court")
	//@NotNull
	private String password;
	private int Tel;
	 private boolean active = false;
	private String photoPath;
	@Temporal(TemporalType.DATE)
	//@JsonFormat(shape=JsonFormat.Shape.STRING, pattern="dd-MM-yyyy")
	private Date Datenaissance;
	//role avec type enum
	//@Enumerated(EnumType.STRING)
	//Role role ;
	//role avec table role 
	/*********/
	@ManyToMany(cascade=CascadeType.ALL, fetch = FetchType.EAGER)
	private List<Role> role = new ArrayList<>();
	@OneToMany(mappedBy="user")
	private List<Publication> publication;
	@OneToMany(mappedBy="user")
	private List<Comment> comment;
	
	
	public List<Comment> getComment() {
		return comment;
	}
	public void setComment(List<Comment> comment) {
		this.comment = comment;
	}
	public List<Publication> getPublication() {
		return publication;
	}
	public void setPublication(List<Publication> publication) {
		this.publication = publication;
	}
	 @OneToMany(mappedBy="user")
	    private List<Claim> claim;
	
	 @OneToMany(mappedBy="user")
	    private List<Ordered> ordered;
	 
	public List<Claim> getClaim() {
		return claim;
	}
	public void setClaim(List<Claim> claim) {
		this.claim = claim;
	}
	public int getUser_id() {
		return User_id;
	}
	public void setUser_id(int user_id) {
		User_id = user_id;
	}
	
	public String getLast_name() {
		return last_name;
	}
	public void setLast_name(String last_name) {
		this.last_name = last_name;
	}
	public String getEmail() {
		return Email;
	}
	public void setEmail(String email) {
		Email = email;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public Date getDatenaissance() {
		return Datenaissance;
	}
	public void setDatenaissance(Date datenaissance) {
		Datenaissance = datenaissance;
	}

	
	public String getUsername() {
		return username;
	}
	public void setUsername(String username) {
		this.username = username;
	}
	public String getAdress() {
		return Adress;
	}
	public void setAdress(String adress) {
		Adress = adress;
	}
	public String getLogin() {
		return Login;
	}
	public void setLogin(String login) {
		Login = login;
	}
	public int getTel() {
		return Tel;
	}
	public void setTel(int tel) {
		Tel = tel;
	}
	
	public List<Role> getRole() {
		return role;
	}
	public void setRole(List<Role> role) {
		this.role = role;
	}
	public List<Ordered> getOrdered() {
		return ordered;
	}
	public void setOrdered(List<Ordered> ordered) {
		this.ordered = ordered;
	}
	
	
	public String getPhotoPath() {
		return photoPath;
	}
	public void setPhotoPath(String photoPath) {
		this.photoPath = photoPath;
	}
	public UserConsomi() {
		super();
		// TODO Auto-generated constructor stub
	}
	public UserConsomi(int user_id,
			@NotEmpty @Size(min = 3, max = 40, message = "nom tres court ressayez a nouveau") String username,
			@Size(min = 6, message = "mots de passe tres court") @NotNull String password, List<Role> role) {
		super();
		User_id = user_id;
		this.username = username;
		this.password = password;
		this.role = role;
	}
	public UserConsomi(@NotEmpty @Size(min = 3, max = 40, message = "nom tres court ressayez a nouveau") String username,
			@NotEmpty @Size(min = 3, max = 40, message = "last name tres court ressayez a nouveau") String last_name,
			String adress, String login, @javax.validation.constraints.Email String email,
			@Size(min = 6, message = "mots de passe tres court") @NotNull String password, int tel, String photoPath,
			Date datenaissance, List<Role> role, List<Publication> publication, List<Comment> comment,
			List<Claim> claim, List<Ordered> ordered) {
		super();
		this.username = username;
		this.last_name = last_name;
		Adress = adress;
		Login = login;
		Email = email;
		this.password = password;
		Tel = tel;
		this.photoPath = photoPath;
		Datenaissance = datenaissance;
		this.role = role;
		this.publication = publication;
		this.comment = comment;
		this.claim = claim;
		this.ordered = ordered;
	}
	public boolean isActive() {
		return active;
	}
	public void setActive(boolean active) {
		this.active = active;
	}
	
	

	
	 
}
