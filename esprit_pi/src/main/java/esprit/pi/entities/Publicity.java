package esprit.pi.entities;

import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
public class Publicity {
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	int idpub;
     private String name;
	
	@Temporal(TemporalType.DATE)
	private Date startdate;
	
	@Temporal(TemporalType.DATE)
	private Date enddate; 
	private int initialtargetnumberofviews;
	private int finalnumberofviews;
	private float advertisingcost;
	private String advertisingtype;
	
	@Enumerated(EnumType.STRING)
	CanalPub canalpub ;
	@ManyToOne(cascade=CascadeType.PERSIST)
	Product product;
	public Product getProduct() {
		return product;
	}

	public void setProduct(Product product) {
		this.product = product;
	}

	public int getIdpub() {
		return idpub;
	}

	public void setIdpub(int idpub) {
		this.idpub = idpub;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Date getStartdate() {
		return startdate;
	}

	public void setStartdate(Date startdate) {
		this.startdate = startdate;
	}

	public Date getEnddate() {
		return enddate;
	}

	public void setEnddate(Date enddate) {
		this.enddate = enddate;
	}

	public int getInitialtargetnumberofviews() {
		return initialtargetnumberofviews;
	}

	public void setInitialtargetnumberofviews(int initialtargetnumberofviews) {
		this.initialtargetnumberofviews = initialtargetnumberofviews;
	}

	public int getFinalnumberofviews() {
		return finalnumberofviews;
	}

	public void setFinalnumberofviews(int finalnumberofviews) {
		this.finalnumberofviews = finalnumberofviews;
	}

	public float getAdvertisingcost() {
		return advertisingcost;
	}

	public void setAdvertisingcost(float advertisingcost) {
		this.advertisingcost = advertisingcost;
	}

	public String getAdvertisingtype() {
		return advertisingtype;
	}

	public void setAdvertisingtype(String advertisingtype) {
		this.advertisingtype = advertisingtype;
	}

	public CanalPub getCanalpub() {
		return canalpub;
	}

	public void setCanalpub(CanalPub canalpub) {
		this.canalpub = canalpub;
	}
	
}
