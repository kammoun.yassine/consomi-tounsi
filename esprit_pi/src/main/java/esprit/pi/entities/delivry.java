package esprit.pi.entities;

import java.util.List;

import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;

@Entity
public class delivry {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	int Delivry_id;
	@Enumerated(EnumType.STRING)
	EtatLiv etatlic ;
	float fraiLiv;
	
	@ManyToOne
    @JoinColumn(name="User_id", nullable=false)
    private Livreur livreur;
	
	  public Livreur getLivreur() {
		return livreur;
	}

	public void setLivreur(Livreur livreur) {
		this.livreur = livreur;
	}

	@OneToOne
	    private Bill bill;

	public int getDelivry_id() {
		return Delivry_id;
	}

	public void setDelivry_id(int delivry_id) {
		Delivry_id = delivry_id;
	}

	public EtatLiv getEtatlic() {
		return etatlic;
	}

	public void setEtatlic(EtatLiv etatlic) {
		this.etatlic = etatlic;
	}

	public float getFraiLiv() {
		return fraiLiv;
	}

	public void setFraiLiv(float fraiLiv) {
		this.fraiLiv = fraiLiv;
	}

	public Bill getBill() {
		return bill;
	}

	public void setBill(Bill bill) {
		this.bill = bill;
	}


	
}