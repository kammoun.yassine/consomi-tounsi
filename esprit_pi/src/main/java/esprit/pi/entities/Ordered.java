package esprit.pi.entities;

import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
@Entity
public class Ordered {
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	int IdCmd;
	@Temporal(TemporalType.DATE)
	Date DateCmd;
	public int getIdCmd() {
		return IdCmd;
	}
	public void setIdCmd(int idCmd) {
		IdCmd = idCmd;
	}
	public Date getDateCmd() {
		return DateCmd;
	}
	public void setDateCmd(Date dateCmd) {
		DateCmd = dateCmd;
	}
	public int getQuantity() {
		return quantity;
	}
	public void setQuantity(int quantity) {
		this.quantity = quantity;
	}

	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}

	
	
	
	int quantity;
	String description;
	
	@OneToMany(mappedBy="ordered",cascade=CascadeType.REMOVE)
	List<Product> ProductList;
	
	public List<Product> getProductList() {
		return ProductList;
	}
	public void setProductList(List<Product> productList) {
		ProductList = productList;
	}




	@ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "User_id")
    public UserConsomi user;
	@OneToOne(cascade = CascadeType.ALL)
	private Bill bill;
	@OneToOne(cascade = CascadeType.ALL)
	private Claim claim;
	
	public Claim getClaim() {
		return claim;
	}
	public void setClaim(Claim claim) {
		this.claim = claim;
	}
	public Bill getBill() {
		return bill;
	}
	public void setBill(Bill bill) {
		this.bill = bill;
	}
	public UserConsomi getUser() {
		return user;
	}
	public void setUser(UserConsomi user) {
		this.user = user;
	}
	
	
}
