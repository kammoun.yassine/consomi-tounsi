package esprit.pi.entities;

import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;

import javax.persistence.OneToOne;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
public class Bill {


	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	int RefFacture;
	@Temporal(TemporalType.DATE)
	Date FactDate;
	float Montent;
	String adrees;
	 @OneToOne
	    @JoinColumn(name = "IdCmd")
	    private Ordered order;
	@OneToOne
	    private delivry delivry;
	
	public delivry getDelivry() {
		return delivry;
	}
	public void setDelivry(delivry delivry) {
		this.delivry = delivry;
	}
	public int getRefFacture() {
		return RefFacture;
	}
	public void setRefFacture(int refFacture) {
		RefFacture = refFacture;
	}
	public Date getFactDate() {
		return FactDate;
	}
	public void setFactDate(Date factDate) {
		FactDate = factDate;
	}
	public float getMontent() {
		return Montent;
	}
	public void setMontent(float montent) {
		Montent = montent;
	}
	public String getAdrees() {
		return adrees;
	}
	public void setAdrees(String adrees) {
		this.adrees = adrees;
	}
	public Ordered getOrder() {
		return order;
	}
	public void setOrder(Ordered order) {
		this.order = order;
	}
	
}
