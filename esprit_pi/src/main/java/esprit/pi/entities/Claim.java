package esprit.pi.entities;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
@Entity
public class Claim {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id_Recl")
	private Integer id_recl;
	
	@Column(name = "description_Recl")
	private String descrip;

	@Enumerated(EnumType.STRING)
	private TypeClaim typereclamation;
	@ManyToOne
	    //@JoinColumn(name="User_id", nullable=false)
	    private UserConsomi user;
	@ManyToOne
	private Ordered ordered;
	@OneToOne
	private Decision decision;
	
	public UserConsomi getUser() {
		return user;
	}

	public void setUser(UserConsomi user) {
		this.user = user;
	}

	public Ordered getOrdered() {
		return ordered;
	}

	public void setOrdered(Ordered ordered) {
		this.ordered = ordered;
	}

	public Integer getId_recl() {
		return id_recl;
	}

	public void setId_recl(Integer id_recl) {
		this.id_recl = id_recl;
	}

	public String getDescrip() {
		return descrip;
	}

	public void setDescrip(String descrip) {
		this.descrip = descrip;
	}

	public TypeClaim getTypereclamation() {
		return typereclamation;
	}

	public void setTypereclamation(TypeClaim typereclamation) {
		this.typereclamation = typereclamation;
	}
	
	
}
