package esprit.pi.entities;

import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
public class Product {
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	int idprod;
	private String name;
	private String description;
	private float price;
	private int amount ; 
	private String photoUrl;
	@Temporal(TemporalType.DATE)
	Date AddedDate;
	@ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "id_cmd")
    public Ordered ordered;
	@ManyToOne(cascade=CascadeType.PERSIST)
	@JoinColumn(name="idcat", nullable=false)
    private Category category;
	@OneToMany(mappedBy="product",cascade=CascadeType.REMOVE)
	List<Publicity> ListPub;
	
	public List<Publicity> getListPub() {
		return ListPub;
	}
	public void setListPub(List<Publicity> listPub) {
		ListPub = listPub;
	}
	public Category getCategory() {
		return category;
	}
	public void setCategory(Category category) {
		this.category = category;
	}
	public Ordered getOrdered() {
		return ordered;
	}
	public void setOrdered(Ordered ordered) {
		this.ordered = ordered;
	}
	public int getIdprod() {
		return idprod;
	}
	public void setIdprod(int idprod) {
		this.idprod = idprod;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public float getPrice() {
		return price;
	}
	public void setPrice(float price) {
		this.price = price;
	}
	public int getAmount() {
		return amount;
	}
	public void setAmount(int amount) {
		this.amount = amount;
	}
	public String getPhotoUrl() {
		return photoUrl;
	}
	public void setPhotoUrl(String photoUrl) {
		this.photoUrl = photoUrl;
	}
	public Date getAddedDate() {
		return AddedDate;
	}
	public void setAddedDate(Date addedDate) {
		AddedDate = addedDate;
	}
	
}
