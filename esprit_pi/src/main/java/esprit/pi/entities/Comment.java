package esprit.pi.entities;



import java.util.Date;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
public class Comment {
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	int idComment;
	
	String description;
	
	@Temporal(TemporalType.DATE)
	Date commentDate;
	
	@Enumerated(EnumType.STRING)
	ReactType reactType ;
	
	@ManyToOne(cascade=CascadeType.PERSIST)
	Publication publication;
	@ManyToOne(cascade=CascadeType.PERSIST)
	UserConsomi user;
	public UserConsomi getUser() {
		return user;
	}

	public void setUser(UserConsomi user) {
		this.user = user;
	}

	public Publication getPublication() {
		return publication;
	}

	public void setPublication(Publication publication) {
		this.publication = publication;
	}

	public int getIdComment() {
		return idComment;
	}

	public void setIdComment(int idComment) {
		this.idComment = idComment;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public Date getCommentDate() {
		return commentDate;
	}

	public void setCommentDate(Date commentDate) {
		this.commentDate = commentDate;
	}

	public ReactType getReactType() {
		return reactType;
	}

	public void setReactType(ReactType reactType) {
		this.reactType = reactType;
	}
	
}
