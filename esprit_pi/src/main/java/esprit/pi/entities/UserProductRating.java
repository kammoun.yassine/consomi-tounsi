package esprit.pi.entities;

import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;
import java.io.Serializable;

@Entity()
@Table(name = "user_product_rate", uniqueConstraints = {
@UniqueConstraint(columnNames = {"id", "product_asin"})})
@EntityListeners(AuditingEntityListener.class)
public class UserProductRating implements Serializable {
    @EmbeddedId
    private UserProductRatingKey User_id;

    @ManyToOne
    @JsonIgnore
    @MapsId("id")
    @JoinColumn(name = "id")
    private User user;
 
    @ManyToOne
    @MapsId("product_asin")
    @JoinColumn(name = "product_asin")
    private Product product;

    @Column(name = "rate")
    private int rate;

    public UserProductRating() {
    }

    public UserProductRating(User user, Product product) {
        this.user = user;
        this.product = product;
    }


    public UserProductRatingKey getId() {
        return User_id;
    }


    public void setId(UserProductRatingKey User_id) {
        this.User_id = User_id;
    }


    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }


    public Product getProduct() {
        return product;
    }


    public void setProduct(Product product) {
        this.product = product;
    }

    public int getRate() {
        return rate;
    }


    public void setRate(int rate) {
        this.rate = rate;
    }
    @Override
    public String toString() {
        JSONObject response = new JSONObject();
        try {
			response.put("ASIN", String.valueOf(getProduct().getIdprod()));
			response.put("rate", String.valueOf(getRate()));

	        return response.toString();
		} catch (JSONException e) {
			 
			e.printStackTrace();
		}
    

        return response.toString();

    }

    

}
