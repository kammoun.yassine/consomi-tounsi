package esprit.pi.entities;

import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;

@Entity
public class Livreur  {
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private int matricule;
	private int moy_trans_liv;
	private boolean dispo_liv;
	private int chargeT_liv;
	private float salaire_liv;
	private float prime_liv;
	private Region region;
	@OneToMany(mappedBy="livreur",cascade=CascadeType.REMOVE)
	List<delivry> Listdelivry;
	public int getMatricule() {
		return matricule;
	}
	public void setMatricule(int matricule) {
		this.matricule = matricule;
	}
	public int getMoy_trans_liv() {
		return moy_trans_liv;
	}
	public void setMoy_trans_liv(int moy_trans_liv) {
		this.moy_trans_liv = moy_trans_liv;
	}
	public boolean isDispo_liv() {
		return dispo_liv;
	}
	public void setDispo_liv(boolean dispo_liv) {
		this.dispo_liv = dispo_liv;
	}
	public int getChargeT_liv() {
		return chargeT_liv;
	}
	public void setChargeT_liv(int chargeT_liv) {
		this.chargeT_liv = chargeT_liv;
	}
	public float getSalaire_liv() {
		return salaire_liv;
	}
	public void setSalaire_liv(float salaire_liv) {
		this.salaire_liv = salaire_liv;
	}
	public float getPrime_liv() {
		return prime_liv;
	}
	public void setPrime_liv(float prime_liv) {
		this.prime_liv = prime_liv;
	}
	public Region getRegion() {
		return region;
	}
	public void setRegion(Region region) {
		this.region = region;
	}
	public List<delivry> getListdelivry() {
		return Listdelivry;
	}
	public void setListdelivry(List<delivry> listdelivry) {
		Listdelivry = listdelivry;
	}
	
	
	
}
