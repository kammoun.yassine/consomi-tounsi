package esprit.pi.entities;

import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToOne;

@Entity
public class Decision {
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	int Iddecision;
	@Enumerated(EnumType.STRING)
	private TypeDecision typedecision;
	@OneToOne(mappedBy="decision")
	private Claim reclamation;
	
	public Claim getReclamation() {
		return reclamation;
	}
	public void setReclamation(Claim reclamation) {
		this.reclamation = reclamation;
	}
	public int getIddecision() {
		return Iddecision;
	}
	public void setIddecision(int iddecision) {
		Iddecision = iddecision;
	}
	public TypeDecision getTypedecision() {
		return typedecision;
	}
	public void setTypedecision(TypeDecision typedecision) {
		this.typedecision = typedecision;
	}
	
}
