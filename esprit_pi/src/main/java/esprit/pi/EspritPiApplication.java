package esprit.pi;





import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.security.servlet.SecurityAutoConfiguration;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

import esprit.pi.repository.UserRepository;


@EnableJpaRepositories(basePackageClasses = UserRepository.class)
@ComponentScan
@SpringBootApplication
(exclude = { SecurityAutoConfiguration.class })
public class EspritPiApplication {

	public static void main(String[] args) {
		SpringApplication.run(EspritPiApplication.class, args);
	}
	@Bean
	public  BCryptPasswordEncoder  getBCPE(){
		return new BCryptPasswordEncoder();
	}
	
}
