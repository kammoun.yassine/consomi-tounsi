package esprit.pi.Repository;
import org.springframework.data.jpa.repository.JpaRepository;
import esprit.pi.entities.Category;

public interface CategoryRepository extends JpaRepository <Category, Integer > {
	 }
