package esprit.pi.Repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.CrudRepository;

import esprit.pi.entities.User;

public interface UserRepository  extends CrudRepository<User,Integer> {

}
