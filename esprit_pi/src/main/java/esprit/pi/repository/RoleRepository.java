package esprit.pi.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import esprit.pi.entities.Role;
@Repository
public interface RoleRepository extends JpaRepository<Role, Integer>{
	  public Role findByRole(String role);
	  
	  //public Role findbyid(int id);
}
