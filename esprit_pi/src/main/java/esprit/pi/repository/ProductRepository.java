package esprit.pi.Repository;
import java.util.List;

 
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import org.springframework.data.jpa.repository.Query;

import esprit.pi.entities.Product;
@Repository
public interface ProductRepository extends  JpaRepository<Product,Integer> {
	 @Query(value = "SELECT * FROM product p WHERE p.category_idcat = ?1", nativeQuery = true)
	    List<Product> findWithId(int id);
}
