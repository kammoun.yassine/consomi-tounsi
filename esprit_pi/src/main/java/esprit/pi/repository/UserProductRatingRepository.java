package esprit.pi.Repository;

import org.springframework.data.jpa.repository.JpaRepository;

import esprit.pi.entities.UserProductRating;

public interface UserProductRatingRepository extends JpaRepository< UserProductRating, Long> {

}
