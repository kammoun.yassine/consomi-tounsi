package esprit.pi.Repository;
import org.springframework.data.jpa.repository.Query;

  
import esprit.pi.entities.Publicity;

import java.sql.Date;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

public interface PublicityRepository extends JpaRepository<Publicity, Integer> {
	@Query(value = "SELECT COUNT(*) FROM user WHERE sexe='FEMME'", nativeQuery = true)
	public int  CountFemmeFromUser();
	@Query(value = "SELECT COUNT(*) FROM User WHERE sexe='HOMME'", nativeQuery = true)
	public int  CountHommeFromUser();
	@Query(value = "SELECT COUNT(*) FROM User WHERE year(NOW())-year(datenaissance) BETWEEN ?1 AND ?2", nativeQuery = true)
	public int  CountUserWithAgeBetwin(int ageDebut,int ageFin);
	@Query(value = "SELECT COUNT(*) FROM User WHERE sexe='FEMME' AND year(NOW())-year(datenaissance) BETWEEN ?1 AND ?2", nativeQuery = true)
	public int  CountUserFemmeWithAgeBetwin(int ageDebut,int ageFin);
	@Query(value = "SELECT COUNT(*) FROM user ", nativeQuery = true)
	public int  CountALLUser();
	@Query(value = "SELECT COUNT(*) FROM User WHERE sexe='HOMME' AND year(NOW())-year(datenaissance) BETWEEN ?1 AND ?2", nativeQuery = true)
	public int  CountUserHommeWithAgeBetwin(int ageDebut,int ageFin);
	@Query(value = "SELECT * FROM publicity WHERE year(NOW())-year(?1) BETWEEN debut_age_cible AND fin_age_cible and gender_cible=?2 and status=1", nativeQuery = true)
	public List <Publicity> getPubForUserConnecter(Date UserDateNaissance,String gendercible);
	@Query(value = "SELECT * FROM publicity WHERE NOW()> enddate and status=1", nativeQuery = true)
	public List<Publicity> findPubFinished();
	@Query(value = "SELECT * FROM publicite WHERE canal LIKE ?1", nativeQuery = true)
	public List<Publicity> findByCanal(String canal);
}
