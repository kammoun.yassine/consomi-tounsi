package esprit.pi.Controller;
 

import java.io.File;
import java.io.IOException;
import java.text.ParseException;
import java.util.Optional;

import javax.mail.MessagingException;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.lang.Nullable;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;
import esprit.pi.entities.Publicity;
import esprit.piService.Publicity.IPublicityService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;

@RestController
@RequestMapping("/publicite")
@Api(value="Publicity Management System", description="Operations pertaining to publicity in Publicity Management System")

public class PublicityController {
	 final 	IPublicityService ipubliciteService;


	    public PublicityController(IPublicityService ipubliciteService) {
	        this.ipubliciteService = ipubliciteService;
	    }
	    @GetMapping("/findpublicities")
	    public ResponseEntity<?> findAll(){
	        return ResponseEntity.ok(ipubliciteService.findAll());
	    }
	@ApiOperation(value = "Add a publicity")
	@PostMapping("/ajouter")
	 public ResponseEntity<?> save(@ApiParam(value = "Employee object store in database table", required = true)
	 @RequestBody Publicity publicity){
		
     Optional<Publicity> publicity1 = Optional.of(ipubliciteService.save(publicity));
            //return ResponseEntity.created(null).body(publicity1);
            return new ResponseEntity<>("Publicity  is created successfully", HttpStatus.CREATED);
         }
	@ApiOperation(value = "Add a publicity with a cost")
	@PostMapping("/ajouter/cout")
	 public ResponseEntity<?> Add(@ApiParam(value = "Employee object store in database table", required = true)@RequestBody Publicity publicity) throws   IOException, ParseException,MessagingException {
          Optional<Publicity> publicity1 = Optional.of(ipubliciteService.Add(publicity) );
          //return new ResponseEntity<>("Publicity  is created successfully", HttpStatus.CREATED);
           return ResponseEntity.created(null).body(publicity1);
     }
	@ApiOperation(value = "Update a publicity")
	 @PutMapping("/update/{idpub}")
	    public ResponseEntity<Object> update(@ApiParam(value = "Update publicity object", required = true)@RequestBody  Publicity publicity, @PathVariable(value = "idpub") int idpub) throws  IOException, ParseException,MessagingException  {

 
	    	publicity.setIdpub(idpub);
	    	
	    	ipubliciteService.Update(publicity,idpub);

	    	return new ResponseEntity<>("Publicity  is updated successfully", HttpStatus.CREATED);
	    }

	@ApiOperation(value = "Delete a publicity ")
	 @DeleteMapping("/delete/{idpub}")
	    public ResponseEntity<?> delete(	@ApiParam(value = "Publicity Id from which publicity object will delete from database table", required = true)@PathVariable("idpub") int idpub){
	        ipubliciteService.deleteById(idpub);
	        //return ResponseEntity.noContent().build();
	        return new ResponseEntity<>("Publicity  is deleted successfully", HttpStatus.CREATED);
	    }
	@ApiOperation(value = "Add a file to the publicity(Image) ")
	 @PostMapping("/file")
	    @ResponseBody
	    public Publicity uploddimg (@RequestParam("file") @Nullable MultipartFile file , @RequestParam("publicite") int idpub ) {
	        Publicity publicite =ipubliciteService.findById(idpub);
	        if(file==null) {
	            publicite.setImage("defaultPic.jpg");
	            ipubliciteService.save(publicite);
	        }else {
	            try {
	                 
  	                File f = new File("C:\\upload\\" +"image" + idpub+file.getOriginalFilename());
	                file.transferTo(f);
	                publicite.setImage("image"+idpub+file.getOriginalFilename());
	                ipubliciteService.save(publicite);
	            } catch (IllegalStateException e) {
 	                e.printStackTrace();
	            } catch (IOException e) {
                  e.printStackTrace();
	            }
	        }

	        return(publicite);
	    }
	

}
