package esprit.pi.Controller;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import esprit.pi.Service.Category.ICategoryService;
import esprit.pi.entities.Category;
import java.util.Optional;

@RestController
@RequestMapping("api/category/")
public class CategoryController {
    final ICategoryService categoryService;



    public CategoryController(ICategoryService categoryService) {
        this.categoryService = categoryService;
    }

    @GetMapping("getcategory")
    public ResponseEntity<?> findAll(){
        return ResponseEntity.ok(categoryService.findAll());
    }

    @GetMapping("{idcat}")
    public ResponseEntity<?> findById(@PathVariable("idcat") int idcat){return ResponseEntity.ok(categoryService.findById(idcat));}

    @PostMapping("addcategory")
    public ResponseEntity<?> save(@RequestBody Category category){

            Optional<Category> category1 = Optional.of(categoryService.save(category));
            return ResponseEntity.created(null).body(category1);


    }

    

    @DeleteMapping("{idcat}")
    public ResponseEntity<?> delete(@PathVariable("idcat") int idcat){
        categoryService.deleteById(idcat);
        return ResponseEntity.noContent().build();
    }
    @PutMapping("/update/{id}")
    public ResponseEntity<Object> update(@RequestBody  Category category, @PathVariable int idcat) {

        category.setIdcat(idcat);
    	
    	categoryService.save(category);

    	return ResponseEntity.noContent().build();
    }
	

}
