package esprit.pi.Controller;

import org.elasticsearch.ResourceNotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import esprit.pi.Repository.ProductRepository;
import esprit.pi.Repository.UserProductRatingRepository;
import esprit.pi.Repository.UserRepository;
import esprit.pi.entities.Product;
import esprit.pi.entities.User;
import esprit.pi.entities.UserProductRating;
import esprit.pi.entities.UserProductRatingKey;

import javax.validation.Valid;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

@RestController
@RequestMapping("api/rates")
public class UserProductRatingController {

    @Autowired
    private UserProductRatingRepository userProductRatingRepository;

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private ProductRepository productRepository;

    @GetMapping(value = "/rateproducts", produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public ResponseEntity<String> getUserRatedProducts(@RequestParam("User_id") Integer User_id)
            throws ResourceNotFoundException {

        User user =
                userRepository
                        .findById(User_id)
                        .orElseThrow(() -> new ResourceNotFoundException("User not found!"));

        return ResponseEntity.ok().body(user.toString());
    }

    /**
     * Update user response entity.
     *
     * @param userId    the user id
     * @param bookRates the rates of the books
     * @return the response entity
     * @throws ResourceNotFoundException the resource not found exception
     */
    @PutMapping(value = "/productrates", consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity rateProducts(
            @RequestParam("User_id")  Integer User_id, @Valid @RequestBody List<HashMap> productRates)
            throws ResourceNotFoundException {

        User user =
                userRepository
                        .findById(User_id)
                        .orElseThrow(() -> new ResourceNotFoundException("User not found!"));

        List<UserProductRating> updatedProductRates = new ArrayList<>();

        for (HashMap map : productRates) {
            Integer productASIN = Integer.parseInt(map.get("asin").toString());
            int rate = Integer.parseInt(map.get("rate").toString());
            Product product =
                    productRepository
                            .findById(productASIN)
                            .orElseThrow(() -> new ResourceNotFoundException("Product not found!"));

            UserProductRating userProductRating = new UserProductRating(user, product);
            UserProductRatingKey userProductRatingKey = new UserProductRatingKey(User_id, productASIN);

            userProductRating.setId(userProductRatingKey);
            userProductRating.setRate(rate);
            updatedProductRates.add( userProductRating);

        }

        return ResponseEntity.ok().body(userProductRatingRepository.saveAll(updatedProductRates).toString());
    }
}
