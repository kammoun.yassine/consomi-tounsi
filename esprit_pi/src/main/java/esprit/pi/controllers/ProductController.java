package esprit.pi.Controller;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
 
import java.io.File;
import java.io.IOException;
import java.util.Optional;
import org.elasticsearch.ResourceNotFoundException;
import org.json.JSONException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.lang.Nullable;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;

import esprit.pi.Repository.UserRepository;
import esprit.pi.Service.Produit.IProduitService;
import esprit.pi.Service.Produit.implementation.ProductService;
import esprit.pi.Service.Produit.implementation.Recommender;
import esprit.pi.entities.Product;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
 

 
	@RestController   
	//Combination of @Controller and @ResponseBody. Beans returned are converted to/from JSON/XML.
	@Api(value="Product Management System", description="Operations pertaining to product in Product Management System")
 
	public class ProductController {
		@Autowired
	    private IProduitService produitService;
		private static final Logger logger = LoggerFactory.getLogger(ProductService.class);
		@Autowired
	   ProductService productService;
	    @Autowired
	    Recommender recommender;
	    @Autowired
	    private UserRepository userRepository;


	    public ProductController(IProduitService produitService) {
	        this.produitService = produitService;
	    }
		@ApiOperation(value = "Get all products with details")
	    @GetMapping("/findproducts")
	    public ResponseEntity<?> findAll(){
	    	logger.debug("inside ProductController.findAll() method");
	    	logger.info("inside ProductController.findAll() method:");
	        return ResponseEntity.ok(produitService.findAll());
	    }
	   
         //@RequestBody to map the student details from request to bean. We are also returning a ResponseEntity with a header containing the URL of the created resource.
		@ApiOperation(value = "Add a product")
	    @PostMapping("/addproduct")
	    public ResponseEntity<?> save(@RequestBody Product product){
	       if(product.BarcodeIsvalid(product.getBarcode())){
	            Optional<Product> product1 = Optional.of(produitService.save(product));
	            //return ResponseEntity.created(null).body(product1);
	            return new ResponseEntity<>("Product is created successfully", HttpStatus.CREATED);
	        } else {
	        	 return new ResponseEntity<>("Only Tunisian products are accepted!please verify your Barcode", HttpStatus.CREATED);
	        }

	    }

		@ApiOperation(value = "Delete a product") 
	    @DeleteMapping("/delete/{idprod}")
	    public ResponseEntity<?> delete(@PathVariable("idprod") int idprod){
	        produitService.deleteById(idprod);
	        return new ResponseEntity<>("Product is deleted successfully", HttpStatus.CREATED);
	    }
		@ApiOperation(value = "Add product file (image)")
	    @PostMapping("/file")
	    @ResponseBody
	    public Product  uploddimg (@RequestParam("file") @Nullable MultipartFile file , @RequestParam("product") int idprod ) {
	        Product product =produitService.findById(idprod);
	        if(file==null) {
	            product.setPhotoUrl("defaultPic.jpg");
	            produitService.save(product);
	        }else {
	            try { 
	                File f = new File("C:\\upload\\" +"image" + idprod+file.getOriginalFilename());
	                file.transferTo(f);
	                product.setPhotoUrl("image"+idprod+file.getOriginalFilename());
	                produitService.save(product);
	            } catch (IllegalStateException e) {
                 e.printStackTrace();
	            } catch (IOException e) {
	                e.printStackTrace();
	            }
	        }

	        return(product);
	    }
		@ApiOperation(value = "Update a product")
	    @PutMapping("/update/{idprod}")
	    public ResponseEntity<Object> update(@RequestBody  Product product, @PathVariable int idprod) {

	    	product.setIdprod(idprod);
	    	
	    	produitService.save(product);

	    	 return new ResponseEntity<>("Product is updated successfully", HttpStatus.CREATED);
	    }
		
		@ApiOperation(value = "Find recommended products for user using KNN Algorithm")
	    @GetMapping(value = "/recommended", produces = MediaType.APPLICATION_JSON_VALUE)
	    public ResponseEntity getRecommendedOroductsByUserId(@RequestParam("User_id") int User_id)
	            throws ResourceNotFoundException, JSONException {
	        if (!userRepository.existsById(User_id)) {
	            throw new ResourceNotFoundException("User not found!");
	        }

	        String recommendedProducts= recommender.recommendedProducts(User_id);
	    	logger.debug("inside ProductController.getRecommendedProductsByUserId() method");

	        return ResponseEntity.ok().body(recommendedProducts);
	    }
	    
	    @PostMapping("/increasePrice")
	    @ResponseBody
	    public void increasePrice(@RequestParam int id) {
	        produitService.increasePrice(id);
	    }

}
