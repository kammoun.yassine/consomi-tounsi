package esprit.pi.Controller;
import java.io.File;
import java.io.IOException;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.lang.Nullable;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import esprit.pi.Service.Produit.IUserService;
import esprit.pi.entities.Product;
import esprit.pi.entities.User;

@RestController
public class UserControllerRest {
	 
	public String welcome() { 
		return "welcome to consomi tounsi."; 
		}

	@Autowired
	IUserService userrep;
	
	@PostMapping("/add")
	@ResponseBody
	public String AddUser(@RequestBody User user)
	{
		userrep.addUser(user);
		return "un utilisateur est creer";
	}
    @PostMapping("/adduser")
    public ResponseEntity<?> save(@RequestBody User user){
            Optional<User> user1 = Optional.of(userrep.save(user));
            return ResponseEntity.created(null).body(user1);
      }
     @GetMapping("/all")
	@ResponseBody
	public List<User> getUsers() {
	List<User> list = userrep.retrieveAllUsers();
	return list;
	}
	 
	@DeleteMapping("/dell/{userid}")  
	private String deleteUser(@PathVariable("userid") int userid)   
	{  
		userrep.deleteUser(userid);
		return "utilisateur suprimer";
	}  
	@PutMapping("/mod-user")
	@ResponseBody
	public User updateEmployee(@RequestBody User user) {
	 return userrep.updateUser(user);
	}
	
	@GetMapping(value ="/user/getAllClient")
	@ResponseBody
	public String gettAllClient(){
		List<User> list =userrep.getAllclient();
		return "listdes clients est"+list;
	}
	@GetMapping(value ="/user/getAllLivreur")
	@ResponseBody
	public String gettAllLivreur(){
		List<User> list =userrep.getAllLiv();
		return "list de livreur est"+list;
	}
	 
	@PostMapping("/img")
    @ResponseBody
    public User  uploddimg (@RequestParam("img") @Nullable MultipartFile img, @RequestParam("author") int User_id ) {
        User user =userrep.findById(User_id);
        if(img==null) {
            user.setPhotoPath("defaultPic.png");
            userrep.save(user);
        }else {
            try {
                ClassLoader classLoader = getClass().getClassLoader();
                String path =  classLoader.getResource(".").getFile();
               
                File f = new File("C:\\upload\\" +"image" + User_id+img.getOriginalFilename());
                img.transferTo(f);
               user.setPhotoPath("image"+User_id+img.getOriginalFilename());
                userrep.save(user);
            } catch (IllegalStateException e) {
           
                e.printStackTrace();
            } catch (IOException e) {
            
                e.printStackTrace();
            }
        }

        return(user);
    }
	//register reemplace adduser 
	@PostMapping("/register")///{idrole}dans l'url et , @PathVariable(name="idrole") int idrole dans la modif 
    public String register(@RequestBody() UserConsomi user ) throws IOException, NexmoClientException {
		//List<Role> ro = new ArrayList<>();
		//userrep.saveUser(user);
		//user=userrep.save(user);
//		 Role ro=role.findById(idrole).orElse(null);
//		user.getRole().add(ro);
        //return userrep.saveUser(user);
//        NexmoClient  client = NexmoClient .builder().apiKey("fcd067ae").apiSecret("FiEE601FB0okLwJj").build();
//        TextMessage message = new TextMessage("Vonage APIs",
//                "21695582311",
//                "un nouveau compte utilisateur a ete creer"
//        );
//
//        SmsSubmissionResponse response = client.getSmsClient().submitMessage(message);
//
//        if (response.getMessages().get(0).getStatus() == MessageStatus.OK) {
//            System.out.println("Message sent successfully.");
//        } else {
//            System.out.println("Message failed with error: " + response.getMessages().get(0).getErrorText());
//        }
		  return userrep.saveUser(user);
    }
	
	@PutMapping(value = "/affecter/{iduser}/{idRole}") 
    public void affecter(@PathVariable("iduser")int iduser,@PathVariable("idRole") int idRole) {
		 Role ro=role.findById(idRole).orElse(null);
		
		use.affecterUserARole(iduser, idRole);	
    }
	//pour la confirmation avec mail
	 @GetMapping(path = "confirm")
	    public String confirm(@RequestParam("token") String token) {
	        return use.confirmToken(token);
	    }
	 //lien de user lorsque il est connecte
		@GetMapping("/user")
		public String User(){
			return ("<h1>welcome</h1>");
		}
		@GetMapping("/admin")
		public String admin(){
			return ("<h1>welcome admin</h1>");
		}
		// recherche des users
		 @GetMapping("/searchuser/{username}")
		  
		  @ResponseBody 
		  public  List<UserConsomi> search(@PathVariable(value = "username") String topic) {
			  List<UserConsomi> list =  userrep.search(topic); 
			  return list; 
		 }

	
	
}
