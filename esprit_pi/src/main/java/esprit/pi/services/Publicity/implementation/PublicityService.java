package esprit.pi.Service.Publicity.implementation;



import java.io.IOException;
import org.springframework.transaction.annotation.Transactional;

import java.sql.Date;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.Locale;

import javax.mail.MessagingException;
import javax.mail.internet.MimeMessage;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.JavaMailSenderImpl;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.stereotype.Service;
import java.util.Calendar;
import java.util.List;
import esprit.pi.Repository.PublicityRepository;
import esprit.pi.entities.Publicity;
 
import esprit.piService.Publicity.IPublicityService;
@Service
public class PublicityService implements IPublicityService {
	@Autowired
	PublicityRepository publicityRepository;
 
	private EmailConfig emailConfig;
 
	public  PublicityService( EmailConfig emailConfig) {
		 this.emailConfig = emailConfig;
	}
	public Publicity save(Publicity p) {
		return publicityRepository.save(p);
	}
	@Transactional 
	public Publicity Add (Publicity pub) throws IOException, ParseException,MessagingException {
		 
		float coutPub = CalculeCoutTotalPub(pub.getGenderCible().toString(), pub.getCanalpub().toString(),
				pub.getDebutAgeCible(), pub.getFinAgeCible(), pub.getStartdate().toString(),
				pub.getEnddate().toString());
		pub.setAdvertisingcost(coutPub);
		pub.setInitialtargetnumberofviews(CountUserCible(pub.getDebutAgeCible(),pub.getFinAgeCible(),pub.getGenderCible().toString()));
		pub.setStatus(true);
		publicityRepository.save(pub);
		  // Create a mail sender
        JavaMailSenderImpl mailSender = new JavaMailSenderImpl();
        mailSender.setHost(this.emailConfig.getHost());
        mailSender.setPort(this.emailConfig.getPort());
        mailSender.setUsername(this.emailConfig.getUsername());
        mailSender.setPassword(this.emailConfig.getPassword());

        // Create an email instance
        SimpleMailMessage mailMessage = new SimpleMailMessage();
        mailMessage.setFrom(pub.getEmailProprietaire());
        mailMessage.setTo(" consommiTounsi619@gmail.com");
        mailMessage.setSubject("New publicity from " + pub.getEmailProprietaire() );
        mailMessage.setText( "Une nouvelle publicité a été créé avec le cout suivant "+pub.getAdvertisingcost());

        // Send mail
        mailSender.send(mailMessage);
    
		return pub ;
		 }
	
	@Override
	public Publicity findById(int idpub) {
		return publicityRepository.getOne(idpub);
	}

	public List<Publicity> findAll() {
		return publicityRepository.findAll();
	}
	@Override

	public void deleteById(int idpub) {
		Publicity p2 = findById(idpub);
		publicityRepository.delete(p2);}
	
		public Publicity Update(Publicity p , int idpub) throws IOException, ParseException,MessagingException {
			Publicity p2 = findById(idpub);
			p2.setCanalpub(p.getCanalpub());
			p2.setName(p.getName());
			p2.setStartdate(p.getStartdate());
			p2.setEnddate(p.getEnddate());
			 
			 
			float coutPub = CalculeCoutTotalPub(p2.getGenderCible().toString(), p2.getCanalpub().toString(),
					p2.getDebutAgeCible(), p2.getFinAgeCible(), p2.getStartdate().toString(),
					p2.getEnddate().toString());
			p2.setAdvertisingcost(coutPub);
			p2.setInitialtargetnumberofviews(CountUserCible(p2.getDebutAgeCible(),p2.getFinAgeCible(),p2.getGenderCible().toString()));
			p2.setStatus(true);
			Publicity PubliciteUpdated = save(p2);
		 
			  // Create a mail sender
	        JavaMailSenderImpl mailSender = new JavaMailSenderImpl();
	        mailSender.setHost(this.emailConfig.getHost());
	        mailSender.setPort(this.emailConfig.getPort());
	        mailSender.setUsername(this.emailConfig.getUsername());
	        mailSender.setPassword(this.emailConfig.getPassword());

	        // Create an email instance
	        SimpleMailMessage mailMessage = new SimpleMailMessage();
	        mailMessage.setFrom(p2.getEmailProprietaire());
	        mailMessage.setTo(" consommiTounsi619@gmail.com");
	        mailMessage.setSubject("publicity updated from " + p2.getEmailProprietaire() );
	        mailMessage.setText( "le nouveau  cout est le  suivant "+p2.getAdvertisingcost());

	        // Send mail
	        mailSender.send(mailMessage);
			return PubliciteUpdated;
		}
		public int NbrUserFemme() {
			return publicityRepository.CountFemmeFromUser();
		}

		public int NbrUserHomme() {
			return publicityRepository.CountHommeFromUser();
		}

		public int NbrUserTotal() {
			return publicityRepository.CountALLUser();
		}

		public int NbrUserAgeBetwin(int ageCibledebut, int ageCibleFin) {
			return publicityRepository.CountUserWithAgeBetwin(ageCibledebut, ageCibleFin);
		}

		public int NbrUserFemmeAgeBetwin(int ageCibledebut, int ageCibleFin) {
			return publicityRepository.CountUserFemmeWithAgeBetwin(ageCibledebut, ageCibleFin);
		}

		public int NbrUserHommeAgeBetwin(int ageCibledebut, int ageCibleFin) {
			return publicityRepository.CountUserHommeWithAgeBetwin(ageCibledebut, ageCibleFin);
		}
		public int CountUserCible(int ageCibledebut, int ageCibleFin, String gendercible) {
			int nbrUser = 0;
			if (gendercible.equals("HOMME")) {
				nbrUser += NbrUserHommeAgeBetwin(ageCibledebut, ageCibleFin);
			}
			if (gendercible.equals("FEMME")) {
				nbrUser += NbrUserFemmeAgeBetwin(ageCibledebut, ageCibleFin);
			}
			if (gendercible.equals("TOUS")) {
				nbrUser += NbrUserAgeBetwin(ageCibledebut, ageCibleFin);
			}

			return nbrUser;
		}

		public float CalculeCoutTotalPub(String gendercible, String canal, int ageCibledebut, int ageCibleFin, String startdate,
				String enddate) throws ParseException {
			float cout = CoutSurCanal(canal);
			if (gendercible.equals("HOMME") || gendercible.equals("FEMME")) {
				cout += 250;
			}
			cout += CoutSurLeNbrDeJour(startdate, enddate);
			cout += CoutSurTrancheAge(ageCibledebut, ageCibleFin);
			 
			return cout;
		}

		public int CoutSurLeNbrDeJour(String startdate, String enddate) throws ParseException {
			if(startdate!=null && enddate!=null){
			int NbrJourPub = DifferenceJourDateDebutEtDateFin(startdate, enddate);
			int cout = 0;
			if (NbrJourPub <= 30) {
				return cout += 5 * NbrJourPub;
			}
			if (NbrJourPub > 30 && NbrJourPub <= 90) {
				return cout += 8 * NbrJourPub;
			}
			if (NbrJourPub > 90 && NbrJourPub <= 180) {
				return cout += 12 * NbrJourPub;
			} else {
				return cout += 20 * NbrJourPub;
			}
			}
			else return 0;

		}
		public int DifferenceJourDateDebutEtDateFin(String startdate, String enddate)   {
			DateTimeFormatter format = DateTimeFormatter.ofPattern("EEE MMM dd HH:mm:ss zzz yyyy", Locale.US);
			LocalDate dateD = LocalDate.parse(startdate, format);
			LocalDate dateF = LocalDate.parse(enddate, format);
			int periodJour;
			int yearDef = dateF.getYear() - dateD.getYear();
			if (yearDef == 0) {
				periodJour = dateF.getDayOfYear() - dateD.getDayOfYear();
				return periodJour;
			} else {
				int nbranne = yearDef * 365;
				periodJour = (dateF.getDayOfYear() - dateD.getDayOfYear()) + nbranne;
				return periodJour;
			}
		}
		public int CoutSurCanal(String canal) {
			if (canal.equals("SITE_ET_FACEBOOK")) {
				return 300;
			}
			if (canal.equals("SITE_ET_TWITTER")) {
				return 150;
			} else
				return 100;
		}
       
		public int CoutSurTrancheAge(int ageCibledebut, int ageCibleFin) {
			if(ageCibledebut!=0 && ageCibleFin!=0){
				int TrancheAge = ageCibleFin - ageCibledebut;
				int cout = 500;
				for (int i = 1; i <= TrancheAge; i++) {
					cout -= 10;
				}
				return cout;
			}
			else return 0;
		}
		
		public List<Publicity> findByCanal(String canal) {
			return publicityRepository.findByCanal(canal);
		}
		public Date ConvertirDate(String date) throws ParseException{
			SimpleDateFormat sdf1 = new SimpleDateFormat("MM/dd/yyyy");
			Calendar calendar = Calendar.getInstance();
			calendar.setTime(sdf1.parse(date));
			calendar.add(Calendar.DATE, 1);
	        return (Date) calendar.getTime();
		}
		
		public List<Publicity> getPubForUserConnecter(Date UserDateNaissance, String gendercible) {
			return publicityRepository.getPubForUserConnecter(UserDateNaissance, gendercible);
		}
		
		 

}