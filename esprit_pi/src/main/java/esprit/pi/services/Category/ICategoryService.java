package esprit.pi.Service.Category;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import esprit.pi.entities.Category;

import java.util.List;
 
public interface ICategoryService {
	public Category save(Category category);
    public Category findById(int idcat);
    public List<Category> findAll();
    public void deleteById(int idcat);
    public Page<Category> findAll(Pageable pageable);

}
