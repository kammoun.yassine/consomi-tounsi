package esprit.pi.services;

public interface EmailSender {
	void send(String to , String email);
}
