package esprit.pi.services;

import java.util.List;

import esprit.pi.entities.Role;

import esprit.pi.entities.UserConsomi;


public interface IUserService {
	
	
	 List<UserConsomi> retrieveAllUsers();
	 public int addUser(UserConsomi user);
	 UserConsomi updateUser(UserConsomi user);
	//User retrieveUser(Roletype role);
	 void deleteUser(int userid);
	 List<UserConsomi> getAllclient();
	 List<UserConsomi> getAllLiv();
	 public UserConsomi save(UserConsomi user);
	 /****new register**/
	 public Role save(Role role);
	 public UserConsomi loadUserByUsername(String username);
	 public void addRoleToUser(String userName,String role);
	
	 public String saveUser(UserConsomi user);
	 
	 public UserConsomi findById(int User_id);
	 public List<UserConsomi> search(String topic);
}
 