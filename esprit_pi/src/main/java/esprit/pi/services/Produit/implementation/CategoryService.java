package esprit.pi.Service.Produit.implementation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import esprit.pi.Repository.CategoryRepository;
import esprit.pi.Service.Category.ICategoryService;
import esprit.pi.entities.Category;
import java.util.List;

@Service
public class CategoryService implements ICategoryService {
	final CategoryRepository categoryRepository;
	 @Autowired
	    public CategoryService(CategoryRepository categoryRepository) {
	        this.categoryRepository = categoryRepository;
	    }

	    @Override
	    public Category save(Category category) {
	        return categoryRepository.save(category);
	    }

	    @Override
	    public Category findById(int idcat) {
	        return  categoryRepository.findById(idcat).orElse(null);
	    }

	    @Override
	    public List<Category> findAll() {
	        return categoryRepository.findAll();
	    }

	    @Override
	    public void deleteById(int idcat) {
	        categoryRepository.deleteById(idcat);

	    }

	    @Override
	    public Page<Category> findAll(Pageable pageable) {
	        return categoryRepository.findAll(pageable);
	    }




}
