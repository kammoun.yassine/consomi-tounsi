package esprit.pi.Service.Produit.implementation;

import java.util.List;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import esprit.pi.Repository.UserRepository;
import esprit.pi.Service.Produit.IUserService;
import esprit.pi.entities.Role;
import esprit.pi.entities.UserConsomi;
@Service
public class UserService implements IUserService {
	@Autowired
	UserRepository userRepository;
	
	@Override
	public List<User> retrieveAllUsers() {
		return (List <User>) userRepository.findAll();
	}
	  
	   @Override
	    public User save(User user) {
	        return  userRepository.save(user);
	    }
	@Override
	public int addUser(User user) {
		userRepository.save(user);
		return user.getUser_id();
	}

	@Override
	public User updateUser(User user) {
		return userRepository.save(user);
	}

	@Override
	public User retrieveUser(Role role) {
      return null;
	}

	@Override
	public void deleteUser(int userid) {
		userRepository.deleteById(userid);
		
	}
	 

	 

	@Override
	public List<User> getAllclient() {
		 return null;
	}

	@Override
	public List<User> getAllLiv() {
		 return null;
	}

	 
	@Override
	public User findById(int User_id) {

		return userRepository.findById(User_id).orElse(null);
	}


}
