package esprit.pi.Service.Produit.implementation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired; 
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import esprit.pi.entities.Product;
import esprit.pi.Repository.ProductRepository;
import esprit.pi.Service.Produit.IProduitService;
import java.util.List;

@Service
public class ProductService implements IProduitService {
	final ProductRepository  productRepository;
 
	 @Autowired
	    public ProductService(ProductRepository productRepository) {
	        this.productRepository = productRepository;
	    }
	 
	 @Override
	    public Product save(Product product) {
	        return  productRepository.save(product);
	    }

	    @Override
	    public Product findById(int idprod) {
	        return  productRepository.findById(idprod).orElse(null);
	    }
		private static final Logger logger = LoggerFactory.getLogger(ProductService.class);
	    @Override
	    public List<Product> findAll() {
	    	logger.debug("inside findAll() method");
	        return productRepository.findAll();
	    }

	    
	    @Override
	    public void deleteById(int idprod) {
	        productRepository.deleteById(idprod);

	    }

	    @Override
	    public Page<Product> findAll(Pageable pageable) {
	        return productRepository.findAll(pageable);
	    }
	    @Override
	    public List<Product> findWithId(int id) {
	        return  productRepository.findWithId(id);

	    }
	    
	    @Override
	    public void increasePrice(int id) {
	        Product prod = productRepository.findById(id).orElse(null);
	        prod.setPrice(prod.getPrice() +
	          prod.getPrice() * 2);
	        productRepository.save(prod);
	    }
	    
}
