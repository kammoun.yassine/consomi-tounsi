package esprit.pi.Service.Produit;
import java.util.List;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import esprit.pi.entities.Product;

public interface IProduitService {
	public Product save(Product product);
   
    public List<Product> findAll();
    public void deleteById(int idprod);
    public Page<Product> findAll(Pageable pageable);
	public Product findById(int idprod);
	public void increasePrice(int id);
	List<Product> findWithId(int id );
	
	 
}
