package esprit.pi.Service.Produit;
import java.util.List;
import esprit.pi.entities.Role;
import esprit.pi.entities.User;

public interface IUserService {
	 List<User> retrieveAllUsers();
	 public int addUser(User user);
	 User updateUser(User user);
	 User retrieveUser(Role role);
	 void deleteUser(int userid);
	 List<User> getAllclient();
	 List<User> getAllLiv();
	 public User save(User user);
	 public User findById(int User_id);
	//  public List<User> findAll();
	 

}
