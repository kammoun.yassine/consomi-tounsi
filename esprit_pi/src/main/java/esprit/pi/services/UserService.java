package esprit.pi.services;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import esprit.pi.entities.ConfirmationToken;
import esprit.pi.entities.Role;

import esprit.pi.entities.UserConsomi;
import esprit.pi.repository.RoleRepository;
import esprit.pi.repository.UserRepository;

@Service
public class UserService implements IUserService {

	@Autowired
	UserRepository userRepository;
	@Autowired
	RoleRepository rolerep;
	@Autowired
    BCryptPasswordEncoder bCryptPasswordEncoder;
	@Autowired
    ConfirmationTokenService1 confirmationTokenService;
	@Autowired
	EmailSender emailSender;
	@Override
	public int  addUser(UserConsomi user) {
		userRepository.save(user);
		return user.getUser_id();
	}

	@Override
	public UserConsomi updateUser(UserConsomi user) {
		return userRepository.save(user);
	}


	@Override
	public void deleteUser(int userid) {
		userRepository.deleteById(userid);
		
	}
	/*
	@Override
	public List<User> getAllclient() {
	
		return userRepository.client();
	}*/
	/*

	@Override
	public List<User> getAllLiv() {
		return userRepository.Liv();
	
	}*/

	@Override
	public UserConsomi save(UserConsomi user) {
		
		return userRepository.save(user);
	}

	@Override
	public UserConsomi findById(int User_id) {

		return userRepository.findById(User_id).orElse(null);
	}

	@Override
	public Role save(Role role) {
		 return rolerep.save(role);
	}

	@Override
	public UserConsomi loadUserByUsername(String username) {
		return userRepository.findByUsername(username);
	}

	@Override
	public void addRoleToUser(String userName, String role) {
		 UserConsomi User=userRepository.findByUsername(userName);
	        Role appRole=rolerep.findByRole(role);
	        User.getRole().add(appRole);
	}
	
	
	/*@Transactional
	public void affecterUserARole(int iduser, int idRole) {
		UserConsomi UserConsomi =userRepository.findById(iduser).get();
		Role role=rolerep.findById(idRole).get();
	if(role.getUser() == null){

			List<UserConsomi> onsmi = new ArrayList<>();
			onsmi.add(UserConsomi);
			role.setUser(onsmi);
		}
	else{

		role.getUser().add(UserConsomi);
		}

		 
	rolerep.save(role); 

	}*/

	@Override
	public String saveUser(UserConsomi user) {
		  String hashPW =bCryptPasswordEncoder.encode(user.getPassword());
		     user.setPassword(hashPW);
		     userRepository.save(user);
		     String token = UUID.randomUUID().toString();
		     ConfirmationToken confiramationtoken = new ConfirmationToken(
		    		token,
		    		LocalDateTime.now(),
		    		LocalDateTime.now().plusMinutes(15),
		    		user
		    		
		    		 );
		     confirmationTokenService.saveConfiramationtoken(confiramationtoken);
		     
		     String link = "http://localhost:8081/confirm?token=" + token;
		     emailSender.send(
		    		 user.getEmail(),
		             buildEmail(user.getUsername(), link));    	
		          return token;
	}
	@Transactional
    public String confirmToken(String token) {
        ConfirmationToken confirmationToken = confirmationTokenService
        		.getToken(token);
        		
        if (confirmationToken.getConfirmedAt() != null) {
            throw new IllegalStateException("email already confirmed");
        }

        LocalDateTime expiredAt = confirmationToken.getExpiresAt();

        if (expiredAt.isBefore(LocalDateTime.now())) {
            throw new IllegalStateException("token expired");
        }

        confirmationTokenService.setConfirmedAt(token);
        userRepository.activeUser(
                confirmationToken.getUser().getUsername());
        return "confirmed";
    }

	@Override
	public List<UserConsomi> getAllclient() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<UserConsomi> getAllLiv() {
		// TODO Auto-generated method stub
		return null;
	}

	
	private String buildEmail(String name, String link) {
        return "<div style=\"font-family:Helvetica,Arial,sans-serif;font-size:16px;margin:0;color:#0b0c0c\">\n" +
                "\n" +
                "<span style=\"display:none;font-size:1px;color:#fff;max-height:0\"></span>\n" +
                "\n" +
                "  <table role=\"presentation\" width=\"100%\" style=\"border-collapse:collapse;min-width:100%;width:100%!important\" cellpadding=\"0\" cellspacing=\"0\" border=\"0\">\n" +
                "    <tbody><tr>\n" +
                "      <td width=\"100%\" height=\"53\" bgcolor=\"#0b0c0c\">\n" +
                "        \n" +
                "        <table role=\"presentation\" width=\"100%\" style=\"border-collapse:collapse;max-width:580px\" cellpadding=\"0\" cellspacing=\"0\" border=\"0\" align=\"center\">\n" +
                "          <tbody><tr>\n" +
                "            <td width=\"70\" bgcolor=\"#0b0c0c\" valign=\"middle\">\n" +
                "                <table role=\"presentation\" cellpadding=\"0\" cellspacing=\"0\" border=\"0\" style=\"border-collapse:collapse\">\n" +
                "                  <tbody><tr>\n" +
                "                    <td style=\"padding-left:10px\">\n" +
                "                  \n" +
                "                    </td>\n" +
                "                    <td style=\"font-size:28px;line-height:1.315789474;Margin-top:4px;padding-left:10px\">\n" +
                "                      <span style=\"font-family:Helvetica,Arial,sans-serif;font-weight:700;color:#ffffff;text-decoration:none;vertical-align:top;display:inline-block\">Confirm your email</span>\n" +
                "                    </td>\n" +
                "                  </tr>\n" +
                "                </tbody></table>\n" +
                "              </a>\n" +
                "            </td>\n" +
                "          </tr>\n" +
                "        </tbody></table>\n" +
                "        \n" +
                "      </td>\n" +
                "    </tr>\n" +
                "  </tbody></table>\n" +
                "  <table role=\"presentation\" class=\"m_-6186904992287805515content\" align=\"center\" cellpadding=\"0\" cellspacing=\"0\" border=\"0\" style=\"border-collapse:collapse;max-width:580px;width:100%!important\" width=\"100%\">\n" +
                "    <tbody><tr>\n" +
                "      <td width=\"10\" height=\"10\" valign=\"middle\"></td>\n" +
                "      <td>\n" +
                "        \n" +
                "                <table role=\"presentation\" width=\"100%\" cellpadding=\"0\" cellspacing=\"0\" border=\"0\" style=\"border-collapse:collapse\">\n" +
                "                  <tbody><tr>\n" +
                "                    <td bgcolor=\"#1D70B8\" width=\"100%\" height=\"10\"></td>\n" +
                "                  </tr>\n" +
                "                </tbody></table>\n" +
                "        \n" +
                "      </td>\n" +
                "      <td width=\"10\" valign=\"middle\" height=\"10\"></td>\n" +
                "    </tr>\n" +
                "  </tbody></table>\n" +
                "\n" +
                "\n" +
                "\n" +
                "  <table role=\"presentation\" class=\"m_-6186904992287805515content\" align=\"center\" cellpadding=\"0\" cellspacing=\"0\" border=\"0\" style=\"border-collapse:collapse;max-width:580px;width:100%!important\" width=\"100%\">\n" +
                "    <tbody><tr>\n" +
                "      <td height=\"30\"><br></td>\n" +
                "    </tr>\n" +
                "    <tr>\n" +
                "      <td width=\"10\" valign=\"middle\"><br></td>\n" +
                "      <td style=\"font-family:Helvetica,Arial,sans-serif;font-size:19px;line-height:1.315789474;max-width:560px\">\n" +
                "        \n" +
                "            <p style=\"Margin:0 0 20px 0;font-size:19px;line-height:25px;color:#0b0c0c\">Hi " + name + ",</p><p style=\"Margin:0 0 20px 0;font-size:19px;line-height:25px;color:#0b0c0c\"> Thank you for registering. Please click on the below link to activate your account: </p><blockquote style=\"Margin:0 0 20px 0;border-left:10px solid #b1b4b6;padding:15px 0 0.1px 15px;font-size:19px;line-height:25px\"><p style=\"Margin:0 0 20px 0;font-size:19px;line-height:25px;color:#0b0c0c\"> <a href=\"" + link + "\">Activate Now</a> </p></blockquote>\n Link will expire in 15 minutes. <p>See you soon</p>" +
                "        \n" +
                "      </td>\n" +
                "      <td width=\"10\" valign=\"middle\"><br></td>\n" +
                "    </tr>\n" +
                "    <tr>\n" +
                "      <td height=\"30\"><br></td>\n" +
                "    </tr>\n" +
                "  </tbody></table><div class=\"yj6qo\"></div><div class=\"adL\">\n" +
                "\n" +
                "</div></div>";
    }

	@Override
	public List<UserConsomi> retrieveAllUsers() {
		return (List <UserConsomi>) userRepository.findAll();
	}

	@Override
	public List<UserConsomi> search(String topic) {
		List<UserConsomi> list =  (List<UserConsomi>) userRepository.search(topic); 
		  return list;
	}
	@Transactional
	public void affecterUserARole(int iduser, int idRole) {
		UserConsomi UserConsomi =userRepository.findById(iduser).get();
		Role Role=rolerep.findById(idRole).get();
	if(UserConsomi.getRole() == null){

		List<Role> role = new ArrayList<>();
		
		role.add(Role);
		UserConsomi.setRole(role);
		}
	else{

		UserConsomi.getRole().add(Role);
		}

		 
	rolerep.save(Role); 

	}
	
	

}
