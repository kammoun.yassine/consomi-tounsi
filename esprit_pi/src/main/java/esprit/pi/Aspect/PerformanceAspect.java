package esprit.pi.Aspect;

import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Pointcut;
import org.springframework.aop.Advisor;
import org.springframework.aop.aspectj.AspectJExpressionPointcut;
import org.springframework.aop.interceptor.PerformanceMonitorInterceptor;
import org.springframework.aop.support.DefaultPointcutAdvisor;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.EnableAspectJAutoProxy;

import esprit.pi.Service.Produit.implementation.ProductService;
import esprit.pi.entities.Product;

@Configuration
@EnableAspectJAutoProxy
@Aspect
public class PerformanceAspect {
	 @Pointcut(
		      "execution(public String esprit.pi.Service.Produit.implementation.ProductService.findAll(..))"
		    )
		    public void mymonitor() { }
		    
		    @Bean
		    public PerformanceMonitorInterceptor performanceMonitorInterceptor() {
		        return new PerformanceMonitorInterceptor(false);
		    }

		    @Bean
		    public Advisor performanceMonitorAdvisor() {
		        AspectJExpressionPointcut pointcut = new AspectJExpressionPointcut();
		        pointcut.setExpression("esprit.pi.Aspect.PerformanceAspect.mymonitor()");
		        return new DefaultPointcutAdvisor(pointcut, performanceMonitorInterceptor());
		    }
		    @Bean
		    public Product prod(){
		    return new Product(1,50);
		    }
		    
		    
		   
		    
}
