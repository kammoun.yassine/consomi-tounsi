package esprit.pi.Aspect;

import java.util.Date;

import org.aopalliance.intercept.MethodInvocation;
import org.apache.commons.logging.Log;
import org.springframework.aop.interceptor.AbstractMonitoringInterceptor;

public class MyperformanceMonitorInterceptor extends AbstractMonitoringInterceptor {
	public MyperformanceMonitorInterceptor() {
    }

    public MyperformanceMonitorInterceptor(boolean useDynamicLogger) {
            setUseDynamicLogger(useDynamicLogger);
    }

    @Override
    protected Object invokeUnderTrace(MethodInvocation invocation, Log log) 
      throws Throwable {
	 String name = createInvocationTraceName(invocation);
     long start = System.currentTimeMillis();
     log.debug("Method " + name + " execution started at:" + new Date());
     try {
         return invocation.proceed();
     }
     finally {
         long end = System.currentTimeMillis();
         long time = end - start;
         log.debug("Method "+name+" execution lasted:"+time+" ms");
         log.debug("Method "+name+" execution ended at:"+new Date());
         
         if (time > 10){
             log.warn("Method execution longer than 10 ms!");
         }            
     }
 }

}
