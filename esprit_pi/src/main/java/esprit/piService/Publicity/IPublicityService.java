package esprit.piService.Publicity;
import java.io.IOException;
import java.text.ParseException;
import java.util.List;

import javax.mail.MessagingException;

import esprit.pi.entities.Publicity;

public interface IPublicityService {
	public Publicity save(Publicity p);
	public List<Publicity> findAll();
	public Publicity Update(Publicity p,int idpub)throws  IOException, ParseException,MessagingException;;
	public void deleteById(int idpub );
	public Publicity findById(int idpub);
	public Publicity Add(Publicity p)throws  IOException, ParseException,MessagingException;

}
